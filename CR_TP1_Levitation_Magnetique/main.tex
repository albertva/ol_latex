\documentclass{article}
\usepackage[utf8]{inputenc}

\title{TP1: \textbf{Lévitation Magnétique}}
\author{Valentin ALBERT / Naël DOS SANTOS D'AMORE / L3EEA }
\date{4 Mars 2020}

\usepackage{natbib}
\usepackage{graphicx}
\usepackage{float}

\usepackage{amsmath}
\usepackage{geometry}
\geometry{
    a4paper,
    total={170mm,257mm},
    left=20mm,
    top=20mm,
}

\begin{document}

\maketitle

\tableofcontents
\listoffigures
        
\section*{Objectifs de la séance}

    \paragraph{}
        L'objectif de cette séance est d'étudier et de déterminer les constantes de temps d'un système de suspension électromagnétique agissant sur une bille en acier.
        
    \paragraph{}  
        Nous verrons ainsi comment un système instable en boucle ouverte peut devenir stable en boucle fermée. Nous verrons aussi comment simplifier des fonctions de transfert tout en conservant leurs cohérence par rapport au modèle physique. Pour finir, nous trouverons qu'il est possible de négliger des asservissements interne lorsque nous disposons d'un modèle qui permet d'asservir tout le système.
        
\newpage
\section{Préparation}

    \paragraph{}
        1) On détermine la relation liant $I_c$ et $x_b$:
        \begin{eqnarray*}
            \frac{d^2x_b}{dt^2} &=& -\frac{K_m*I_c^2}{2*M_b*x_b^2} + g \\
            \Leftrightarrow -\frac{K_m*I_c^2}{2*M_b*x_b^2} &=& \frac{d^2x_b}{dt^2} - g\\
            \Leftrightarrow I_c^2 &=& -\frac{2*M_b*x_b^2}{K_m}(\frac{d^2x_b}{dt^2} - g)\\
        \end{eqnarray*}
        
        Or \( \displaystyle \frac{d^2x_b}{dt^2} = 0 \) car on recherche la valeur de $I_{c_0}$ lorsque $x_{b_0} = 6mm$. Ce qui signifie que l'on considère le système comme étant stable, donc $x_b = x_{b_0}$ soit une constante qui ne varie pas en fonction du temps.
        
    \paragraph{}
        2) Application numérique:
        \begin{eqnarray*}
            I_{c0} = \sqrt{\frac{2*M_b*x_{b0}^2}{K_m}*g} =                         \sqrt{\frac{2*0.068*0.006^2}{6.5308*10^{-5}}*g} = 0.8576A.
        \end{eqnarray*}
        Ainsi, pour obtenir une distance \(x_{b0}\) = 6mm, il faut que \(I_{c0}\) = 0.08576A.
    \paragraph{}
        3) Calcul de la fonction de transfert $G_m(P)$:
        \begin{eqnarray*}
            \frac{d^2x_b}{dt^2} = \frac{2*g*x_b}{x_{b0}} - \frac{2*g*I_c}{I_{c0}}
        \end{eqnarray*}
        
    \paragraph{}
        Transformée de Laplace:
        \begin{eqnarray*}
            X_b(P^2) &=& \frac{2*g*X_b(P)}{x_{b0}} - \frac{2*g*I_c(P)}{I_{c0}}\\
            X_b(P)*P^2*x_{b0}*I_{c0} - 2*g*X_b(P)*I_{c0} &=& - 2*g*I_c(P)*x_{b0}\\
            X_b(P)(P^2*x_{b0}*I_{c0} - 2*g*I_{c0}) &=& - 2*g*I_c(P)*x_{b0}\\
            G_m(P) = \frac{X_b(P)}{I_c(P)} &=& \frac{- 2*g*x_{b0}}{P^2*x_{b0}*I_{c0} - 2*g*I_{c0}}
        \end{eqnarray*}
        
    \paragraph{}
        On cherche les pôles du dénominateur:
        \begin{eqnarray*}
            \Delta &=& - 4*x_{b0}*I_{c0}*(-2*g*I_{c0}) \\
            &=& 8*g*x_{b0}*I_{c0}^2\\
            &&\{Y_1, Y_2\} = \pm I_{c0}\frac{\sqrt{8*g*x_{b0}}}{2*x_{b0}*I_{c0}}
        \end{eqnarray*}
        
        Avec $x_b{_0}$, $I_c{_0}$ et g réel positif.
        Comme il existe cette condition, on peut conclure que $\{Y_1,Y_2\}$ ne sont pas tout les deux à partie à réelle négative et à partie imaginaire nulle. Or des conditions nécessaire pour dire qu'un système du second ordre est stable est de vérifier si tout les pôles de son dénominateur sont à parties réels négatives. Ce qui signifie que le diagramme de Nyquist de cette fonction coupe l'axe des imaginaires. De plus, il reboucle autour du point critique, car $|Y2|<<-1$, ce qui suffit pour dire que le système est instable.
        
    \paragraph{} 
        4)
        \begin{figure}[H]é
            \centering
            \includegraphics[scale=0.5]{Prepa_BF.PNG}
            \caption{Ball Position PIV-plus-Feedforward Control Loop}
            \label{fig:Prepa_BF.PNG}
        \end{figure}
        
        \begin{eqnarray*}
            I_{c_1} &=& (x_{b_{des}} - x_b)*(K_2 + K_3) - x_b * K_4\\
            I_{c_{des}} &=& I_{c_1} + I_{c_{f}} = I_{c_1} + x_{b_{des}}*K_1 = (x_{b_{des}} - x_b)(K_2 + K_3) - x_b*K_4 + x_{b_{des}}*K_1\\
            x_b &=& I_{c_{des}} * G_m(P)
        \end{eqnarray*}
        
        Avec \(K_1 = K_{ff_b}, K_2 = \frac{K_{i_b}}{P} \), \(K3 = K_{p_b}, K4 = K_{v_b}*P\) et \(G_m(P) = \dfrac{-2*g*x_{b0}}{-2*g*I_{c0} + x_{b0}*I_{c0}*P^2}\)
        On a :
        \begin{eqnarray*}
            X_b(P) &=& [X_b(P)*(-K_2 -K_3 -K_4) + X_{b_{des}}(P)*(K_2 + K_3 + K_1)]*G_m(P)\\
            X_b(P)[1 + G_m(P)(K_2 +K_3 +K_4)] &=& X_{b_{des}}(P)*(K_2 + K_3 + K_1)]*G_m(P)\\
            F(P) = \frac{X_b(P)}{X_{b_{des}}(P)} &=& \frac{G_m(P)*(K_1 + K_2 + K_3)}{1 + G_m(P)(K_2 +K_3 +K_4)}\\
            F(P) &=& \frac{\frac{-2*g*x_{b0}}{-2*g*I_{c0} + x_{b0}*I_{c0}*P^2}*(K_{ff_b}*\frac{K_{i_b}}{P}*K_{p_b})}{1 + \frac{-2*g*x_{b0}}{-2*g*I_{c0} + x_{b0}*I_{c0}*P^2}*(\frac{K_{i_b}}{P} + K_{p_b} + K_{v_b}*P)}\\
            F(P) &=& \frac{-2*g*x_{b0}*(K_{ff_b} + \frac{K_{i_b}}{P} + K_{p_b})}{-2*g*I_{c0} + x_{b0}*I_{c0}*P^2 + (-2*g*x_{b0})*(\frac{K_{i_b}}{P} + K_{p_b} + K_{v_b}*P)}\\
            F(P) &=& \frac{K_{ff_b} + \frac{K_{i_b}}{P} + K_{p_b}}{\frac{I_{c0}}{x_{b0}} + \frac{I_{c0}*P^2}{-2*g} + \frac{K_{i_b}}{P} + K_{p_b} + K_{v_b}*P}\\
            F(P) &=& \frac{-2*g*P*(K_{ff_b} + K_{p_b}) - 2*g*K_{i_b}}{-2*g*K_{i_b} -2*g*P*(\frac{I_{c0}}{x_{b0}} + K_{p_b}) -2*g*K_{v_b}*P^2 + I_{c0}*P^3}\\
        \end{eqnarray*}
        
        
    \paragraph{}
        5) Application numérique:
        
        Avec: \(K_{ff_b} = 142.9\), \(K_{p_b} = -252.6\), \(K_{v_b} = -4.3\) et \(K_{i_b} = -248.1\)
        
        \[F(P) = \frac{4867.722 + 2152.314*P}{4867.722 + 2151.66*P + 84.366*P^2 + 0.8576*P^3}\]
        
        On utilise la fonction \textit{"roots"} de Mathlab afin de trouver les pôles du dénominateur de F(P).
        
        \begin{figure}[H]
            \centering
            \includegraphics[scale=0.8]{valeur_Q6.png}
            \caption{Code Matlab permettant de déterminer les pôles et les zéros de l'équation} 
            \label{fig:valeur_Q6.png}
        \end{figure}
        
        Ainsi, on obtient:
        
        \[F(P) = \frac{P + 2.26}{(P + 2.5)(P + 42.57)(P + 53.30)}\]
        
        On remarque que \(\frac{p + 2.26}{p + 2.5} \simeq 1\) et négligeable comparer à \((p + 42.57)\) et \((p + 53.30)\)
        
        Ainsi, on peut simplifier F(p), ce qui nous donne:
        
        \[F(P) = \frac{1}{(P + 42.57)(P + 53.30)} = \frac{1}{(1 + 0.024P)(1 + 0.018P)}\]

     
\section{Manipulation}
    \paragraph{}
        6) On exécute les fichiers \textit{"setup\_lab\_maglev\_piv.m"} et \textit{"q\_piv\_maglev\_rtw.mdl"} et on observe les courbes suivantes:
        \begin{figure}[H]
            \centering
            \includegraphics[scale=0.5]{Releve_Q6.png}
            \caption{Position de la bille en métal \textit{(en jaune)} par rapport à la commande \textit{(en bleu)} en fonction du temps}
            \label{fig:Releve_Q6.png}
        \end{figure}
        On remarque que la bille oscille autour de la position désirée. Cela s'explique par le fait que nous plaçons la bille à la main et donc, sans le vouloir, nous lui donnons une légère impulsion. Comme le système est oscillant, il n'arrive pas à contrôler cette impulsion initiale. Aussi par le fait que le capteur mesure la distance de la bille de manière échantillonnée, de temps en temps il délivre une valeur fausse au programme. Le fait de transmettre une mauvaise valeur (comme le pic que l'on peut observer à droite de notre courbe) provoque une augmentation du courant délivré dans la bobine. Nous avons pus observé ce phénomène plusieurs fois lors de la séance.
        
        A chaque fois, l'oscillation réduit jusqu'à un minimum sans jamais arriver à s'annuler. D'autant plus que cette dernière se voie s'accroître lorsque le régime transitoire tend à se finir. La succession d'échelon est donc primordiale pour contenir le système dans un état de stabilité.
        
    \newpage
    \paragraph{}
        7) En utilisant l'équation déterminée lors de la question 5, on obtient le schéma suivant:
        \begin{figure}[H]
            \centering
            \includegraphics[scale=0.6]{schema_simulink.PNG}
            \caption{Schéma Simulink pour comparer notre équation par rapport au système étudié}
            \label{fig:schema_simulink.png}
        \end{figure}
        
        Ce schéma nous permet d'obtenir les courbe suivantes:
        \begin{figure}[H]
            \centering
            \includegraphics[scale=0.5]{Releve_Q7_2modeleIdentiques.png}
            \caption{Comparaison entre le modèle de la question 5 \textit{(en rouge)} et les résultats de la question 6 \textit{(en bleu)}}
            \label{fig:Releve_Q7_2modeleIdentiques.png}
        \end{figure}
        On observe que la courbe obtenue grâce à l'équation F(P), mise sous la forme $\frac{1}{(1+t1P)(1+t2P)}$ \textit{(en bleu)}, à une allure similaire à la courbe obtenue grâce au régulateur de la question 3 \textit{(en rouge)}. Afin de réduire ces écarts, on pourrait chercher à affiner les valeurs de $\tau 1$ et $\tau 2$ déterminer lors de la question 5. Cela nous permettrait d'obtenir un écart plus faible entre les 2 courbes mais ce n'était pas le but désiré car nous voulions justement réduire la complexité de l'équation.
        
        Ces courbes nous permettent de confirmer que l'équation F(P) déterminée lors que la question 4 peut-être approchée sous la forme de  $F(P) = \frac{1}{(1+\tau 1P)(1+\tau 2P)}$ car l'écart, même s'il existe, reste totalement admissible pour notre application. En effet, nous avions négligé des constantes de temps de l'ordre de 0.42 secondes devant celles conservées, qui sont de l'ordre de 0.02 secondes. Les constantes de temps nous intéressants étant celles qui induisent les variations à hautes fréquences, il était normal de faire cette approximation pour simplifier le modèle numérique tout en conservant un modèle cohérent à la réalité.

    \paragraph{}
        8) On observe que le correcteur qui régule l'intensité du courant dans la bobine correspond à un correcteur Proportionnel Intégrale (PI).
        On peut calculer la fonction de transfert pour la régulation du courant ainsi:
        \begin{eqnarray*}
            (I_{c_{des}} - I_c) * (K_p + \frac{K_i}{P}) = V_c\\
            V_c = Ic*(L*P + R_c)\\
            I_{c_{des}}*(K_p + \frac{K_i}{P}) = I_c*(L*P + R_c) + I_c*(K_p + \frac{K_i}{P})\\
            \frac{I_c}{I_{c_{des}}} = \frac{K_p + \frac{K_i}{P}}{K_p + \frac{K_i}{P} + L*P + R_c}\\
            \frac{I_c}{I_{c_{des}}} = \frac{K_p*P + K_i}{K_p*P + K_i + R_c*P + L*P^2}\\
            \frac{I_c}{I_{c_{des}}} = \frac{P + \frac{K_i}{K_p}}{1 + P*\frac{K_p + R_c}{K_i} + L*P^2}
        \end{eqnarray*}
        
        On sait que: $K_p = 66.8$, $K_i = 23.3$, $R_c = 10\Omega$ et $L_c = 412.5 mH$
        
        On utilise la fonction \textit{"roots"} de Matlab afin de déterminer les pôles et les zéros de l'équation.
        
        \begin{figure}[H]
            \centering
            \includegraphics[scale=0.8]{valeur_Q7.png}
            \caption{Code Matlab permettant de déterminer les pôles et les zéros de l'équation}
            \label{fig:valeur_Q7.png}
        \end{figure}
        
        Ainsi, on obtient: \(F(P) = \frac{P + 0.3488}{(P + 7.6748)(P + 0.3159)}\)
        
        Or, \(\frac{P + 0.3488}{P + 0.3159} \simeq 1\)
        
        Finalement, on obtient:  \(F(P) = \frac{1}{(P + 7.6748)} = \frac{1}{1 + \tau*P}\)
        
        Avec $\tau = 0.0188$
        
    \paragraph{}
        
        9) Cette boucle de régulation peut être négligée dans la conception de la boucle de régulation principal car on remarque que la boucle de régulation sert à gérer le courant dans la bobine, or ce courant varie très faiblement. En effet, la boucle de régulation principale fait varier l'intensité dans la bobine en fonction de l'écart entre \(x_{b_des}\) et \(x_b\). Cet écart est faible, donc la variation d'intensité est faible, et le générateur peut donc suivre les faibles pics de courant qu'on lui demande de fournir. De plus, la constante de temps de cette fonction est similaire en ordre de grandeur à celle de l'asservissement principale.
        
        Tout ceci nous montre bien que la boucle de régulation est négligeable par rapport à la boucle de régulation principale. Cependant, on pourrait imaginer qu'elle pourrait servir de sécurité pour le matériel si jamais une variation trop grande de l'intensité viendrait à se produire (car cette régulation génère des effets de seuils si la commande est trop élevée).


    \section{Conclusion}
    
    \paragraph{}
        Pour conclure, ce TP nous aura permis d'apprendre à étudier un système pour définir son équation, puis la simplifier si cela est possible. Nous avons vu comment se mettait en oeuvre les correcteurs proportionnels intégrale et en quoi il nous permettait de simplifier nos fonctions de transferts (par la compensation d'une constante de temps).
    
    \paragraph{}
        Pour la simplification des fonctions de transfert, l'utilisation de la fonction \textit{"roots"} de Matlab est un plus très appréciable car elle nous permet de déterminer les pôles et les zéros d'une équation d'ordre n rapidement et précisément.
  
\end{document}
